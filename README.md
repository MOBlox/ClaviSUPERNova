**How to run without auto-restarting**

1. Make sure you have NPM and Node installed.
2. In the folder, run `npm i`
3. Plug in your device to your Clavinova using a USB to Host cable (Commonly used with a Printer)
4. In the folder, run `sudo node .`
5. In your Web Browser, go to http://localhost

**How to run with auto-restarting using PM2**
*This method is best if you want to leave it running*

1. Make sure you have NPM and Node installed.
2. Run `npm i -g pm2`
3. In the folder, run `npm i`
4. Plug in your device to your Clavinova using a USB to Host cable (Commonly used with a Printer)
5. In the folder, run `sudo pm2 start . --name clav`
6. In your Web Browser, go to http://localhost
7. To restart the program, run `sudo pm2 restart clav` or hold down the left and right pedals

**FAQs**

Q: Why do you need to run the program as root?  
A: It uses port 80, which can only be used by root.

Q: Why should I use pm2?  
A: It will automatically restart the program when it crashes, or you close it.

Q: Can I unplug the Clavinova whilst the program is running?  
A: Yes. The program detects connections and disconnections, and prevents crashes.