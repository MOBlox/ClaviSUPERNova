var express = require('express');
var app = express();
var fs = require('fs');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var shell = require('shelljs');
var midi = require('midi');
var debug = false;
var time = Date.now();
var httpPort = 80;
var midiPort = 1;
var modes = ['Off','Repeat','Linthesia'];
var m = {};
var dlog = [];
var ddelta = 0;
var connected = null;
var rped = false;
var input = new midi.input();
var output = new midi.output();
var toWhite = [2,1,2,1,1,2,1,2,1,2,1,1];
function oK(key){
	//Octave Key
	return (key) % 12;
}
Array.prototype.eq = function(arr){
	console.log(this);
	for(var i = 0;i < arr.length;i++){
		for(var j = 0;j < arr[i].length;j++){
			if(this[i] && !this[i].includes(arr[i][j])){
				return false;
			}
		}
	}
	return true;
}

app.all("/", function(req, res){
	// res.send('hi');
	res.send(fs.readFileSync('./index.html', 'utf8'));
});
app.all("/project", function(req, res){
	// res.send('hi');
	res.send(fs.readFileSync('./project.html', 'utf8'));
});

var httpServer = server.listen(httpPort, function () {
	console.log('Server listening at port %d', httpPort);
});
  
  
io.on('connection', function (socket) {
	socket.emit('m', m);
	socket.on('mode',function(e){
		m[e] = !m[e];
		io.emit('m', m);
	});
	socket.on('message',function(e){
		output.sendMessage(e);
	});
});

function checkPort() {
	setTimeout(checkPort, 5000);
	if(input.getPortCount() > midiPort && output.getPortCount() > midiPort && input.getPortName(midiPort).includes('Clavinova') && output.getPortName(midiPort).includes('Clavinova')){
		if(!connected){
			m.connect = connected;
			portSucc();
		}
		connected = true;
		m.connect = connected;
	} else {
		if(connected === null){
			console.log("Input Device  | Not Connected | None");
			console.log("Output Device | Not Connected | None");
			connected = false;
			m.connect = connected;
		}
		if(connected){
			connected = false;
			m.connect = connected;
			output.closePort();
			input.closePort();
			console.log("Input Device  | Disconnected  | None");
			console.log("Output Device | Disconnected  | None");
		}
	}
	io.emit('m', m);
}
checkPort();
function portSucc(){
	input = new midi.input();
	output = new midi.output();
	console.log("Input Device  | Connected     | " + input.getPortName(midiPort));
	console.log("Output Device | Connected     | " + output.getPortName(midiPort));
	output.openPort(midiPort);
	input.openPort(midiPort);
	input.ignoreTypes(false, false, true);
	input.on('message', function(deltaTime, message) {
		ddelta += deltaTime;
		if(m.func){ //Function Menu
			if(message[0] == 128){
				m.func = false;
				switch(message[1]) {
					case 21:
						m = {connect: connected};
						break;
					case 22:
						m.repeat = !m.repeat;
						break;
					case 23:
						m.linthe = !m.linthe;
						break;
					case 24:
						m.ptenth = !m.ptenth;
						break;
					case 25:
						m.poctav = !m.poctav;
						break;
					case 26:
						m.silenc = !m.silenc;
						break;
					case 96:
						shell.exec(`pidof -x linthesia-master || linthesia-master`, {async:true});
						break;
					case 97:
						shell.exec(`pkill linthesia`, {async: true});
						break;
					case 107:
						shell.exec(`xdotool key Escape`);
						break;
					case 108:
						shell.exec(`xdotool key Return`);
						break;
				}
				io.emit('m',m);
			}
			return;
		}
		if(message[0] == 144) {
			if(dlog[0] && ddelta < 0.03){
				dlog[0].unshift(oK(message[1]));
			} else {
				dlog.unshift([oK(message[1])]);
			}
			ddelta = 0;
		}

		if(m.repeat){ //Repeat Mode
			if(message[0] == 144){
				setTimeout(() => {output.sendMessage([144,message[1],message[2]])},1000);
			}
			if(message[0] == 128){
				setTimeout(() => {output.sendMessage([128,message[1],message[2]])},1000);
			}
		}

		if(m.ptenth){ //Plus a Tenth Mode
			if(message[0] == 144){
				output.sendMessage([144,message[1] + 12 + toWhite[oK(message[1])] + toWhite[oK(message[1] + toWhite[oK(message[1])])],message[2]]);
			}
			if(message[0] == 128){
				output.sendMessage([128,message[1] + 12 + toWhite[oK(message[1])] + toWhite[oK(message[1] + toWhite[oK(message[1])])],message[2]]);
			}
		}

		if(m.poctav){ //Plus an Octave Mode
			if(message[0] == 144){
				output.sendMessage([144,message[1] + 12,message[2]]);
			}
			if(message[0] == 128){
				output.sendMessage([128,message[1] + 12,message[2]]);
			}
		}
		if(m.linthe){ //Linthesia Mode
			if(message[0] == 144){
				if(dlog.length > 6 && dlog.slice(0,7).eq([[5],[7],[0],[5],[9],[10],[0]])){
					shell.exec(`pidof -x linthesia-master || linthesia-master	/home/moblox/Midi/Games/SMG\\ -\\ Purple\\ Comet.mid`, {async:true});
				}
				if(dlog.length > 6 && dlog.slice(0,7).eq([[0],[2],[0],[5],[9],[10],[0]])){
					shell.exec(`pidof -x linthesia-master || linthesia-master	/home/moblox/Midi/Games/SMG\\ -\\ Purple\\ Comet.mid`, {async:true});
				}
				if(dlog.length > 7 && dlog.slice(0,8).eq([[3],[0],[8],[10],[0],[2],[3],[3]])){
					shell.exec(`pidof -x linthesia-master || linthesia-master	/home/moblox/Music/sjrs-v4.mid`, {async:true});
				}
				if(dlog.length > 5 && dlog.slice(0,6).eq([[7],[2],[0],[8],[3],[8]])){
					shell.exec(`pidof -x linthesia-master || linthesia-master	/home/moblox/Music/sjrs-v4.mid`, {async:true});
				}
				if(dlog.length > 7 && dlog.slice(0,6).eq([[4],[6],[7],[9],[11],[2],[0],[11]])){
					shell.exec(`pidof -x linthesia-master || linthesia-master	/home/moblox/Midi/Beethoven/ecossaise.mid`, {async:true});
				}
			}
		}

		if(m.silenc){
			if(message[0] == 144){
				output.sendMessage([message[0],message[1],0]);
				console.log('sending off');
			}
		}

		if(debug) console.log(message[0] + " | " + message[1] + " | " + message[2] + " | " +  deltaTime);
		if(message[0] == 176 && message[1] ==  67 && message[2] == 127){
			time = Date.now();
		}
		if(message[0] == 176 && message[1] == 67 && message[2] == 0){
			if(Date.now() - time > 2000){
				if(!rped){
					//Toggle Debug Mode
					debug = !debug;
				} else {
					//Exit Program
					close();
				}
			} else {
				//Toggle Function Menu
				m.func = !m.func;
				io.emit('m',m);
			}
		}

		//Right Pedal position detection
		if(message[0] == 176 && message[1] == 64 && message[2] == 0){
			//Right Pedal Up
			rped = false;
		}
		if(message[0] == 176 && message[1] == 64 && message[2] == 127){
			//Right Pedal Up
			rped = true;
		}
	});
}
function close(){
	output.closePort();
	input.closePort();
	if(connected){
		httpServer.close();
		process.exit()
	}
}